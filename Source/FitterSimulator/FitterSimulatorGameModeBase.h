// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FitterSimulatorGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class FITTERSIMULATOR_API AFitterSimulatorGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
