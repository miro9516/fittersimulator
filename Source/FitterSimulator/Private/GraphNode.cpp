// Fill out your copyright notice in the Description page of Project Settings.


#include "GraphNode.h"
#include "Graph.h"
#include "GraphsContainer.h"

//Initialization of UGraphNode
//////////////////////////////////////////////////////////////////////////////////////////
UGraphNode* UGraphNode::Create(UGraph* NewParentGraph)
{
	return UGraphNode::Create(NewParentGraph, nullptr);
}

UGraphNode* UGraphNode::Create(UGraph* NewParentGraph, UObject* NewData)
{
	UGraphNode* GraphNode = NewObject<UGraphNode>();
	GraphNode->Init(NewParentGraph, NewData);
	return GraphNode;
}

void UGraphNode::Init(UGraph* NewParentGraph, UObject* NewData)
{
	ParentGraph = NewParentGraph;
	Data = NewData;
	ParentGraph->AddNode(this);
}
/////////////////////////////////////////////////////////////////////////////////////////

void UGraphNode::Connect(UGraphNode* Node)
{
	Bonds.Add(Node);
	Node->Bonds.Add(this);
	if (Node->GetParentGraph() != ParentGraph)
	{
		auto NodesofNodeParentGraph = Node->GetParentGraph()->GetNodesPtr();
		if (NodesofNodeParentGraph->Num() > ParentGraph->GetNodesPtr()->Num())
		{
			TransferNodesToGraph(ParentGraph, Node->GetParentGraph());
		}
		else
		{
			TransferNodesToGraph(Node->GetParentGraph(), ParentGraph);
		}
	}
}

void UGraphNode::TransferNodesToGraph(UGraph* FromGraph, UGraph* ToGraph)
{
	for (auto Elem : *FromGraph->GetNodesPtr())
	{
		ToGraph->AddNode(Elem);
		Elem->SetParentGraph(ToGraph);
	}
	FromGraph->GetNodesPtr()->Empty();
	FromGraph->GetGraphsContainer()->GetGraphs()->Remove(FromGraph);
}

void UGraphNode::SetParentGraph(UGraph* NewParentGraph)
{
	ParentGraph = NewParentGraph;
}

void UGraphNode::Disconnect(UGraphNode* Node)
{
	Bonds.Remove(Node);
	Node->Bonds.Remove(this);
	ParentGraph->NormalizeGraph();
}

UGraph* UGraphNode::GetParentGraph()
{
	return ParentGraph;
}


TSet<UGraphNode*>* UGraphNode::GetBonds()
{
	return &Bonds;
}

UObject* UGraphNode::GetData()
{
	return Data;
}
