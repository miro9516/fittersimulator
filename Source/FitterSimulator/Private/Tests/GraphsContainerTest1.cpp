#include "CoreTypes.h"
#include "Containers/UnrealString.h"
#include "GraphsContainer.h"
#include "Graph.h"
#include "GraphNode.h"
#include "Misc/AutomationTest.h"

#if WITH_DEV_AUTOMATION_TESTS

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FGraphsContainerTest1, "Graphs.GraphsContainerTest1", EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)

bool FGraphsContainerTest1::RunTest(const FString& Parameters)
{
	//Test for subtract Graphs
	{
		UGraphsContainer* t1_GraphsContainer = NewObject<UGraphsContainer>();

		UGraph* t1_Graph = UGraph::Create(t1_GraphsContainer);

		UGraphNode* t1_Node1 = UGraphNode::Create(t1_Graph);
		UGraphNode* t1_Node2 = UGraphNode::Create(t1_Graph);
		UGraphNode* t1_Node3 = UGraphNode::Create(t1_Graph);
		UGraphNode* t1_Node4 = UGraphNode::Create(t1_Graph);

		t1_Node1->Connect(t1_Node2);
		t1_Node2->Connect(t1_Node3);
		t1_Node3->Connect(t1_Node4);
		t1_Node2->Disconnect(t1_Node3);

		TestEqual(TEXT("Correct Count Of Nodes in Graph"), t1_Graph->GetNodesPtr()->Num(), 2);
		TestTrue(TEXT("Subtract Graph != Original Graph"), t1_Node2->GetParentGraph() != t1_Node3->GetParentGraph());
		TestEqual(TEXT("Correct Count Of Graphs"), t1_GraphsContainer->GetGraphs()->Num(), 2);
	}

	//Test for subtract looped Graph
	{
		UGraphsContainer* t2_GraphsContainer = NewObject<UGraphsContainer>();

		UGraph* t2_Graph = UGraph::Create(t2_GraphsContainer);

		UGraphNode* t2_Node1 = UGraphNode::Create(t2_Graph);
		UGraphNode* t2_Node2 = UGraphNode::Create(t2_Graph);
		UGraphNode* t2_Node3 = UGraphNode::Create(t2_Graph);
		UGraphNode* t2_Node4 = UGraphNode::Create(t2_Graph);

		t2_Node1->Connect(t2_Node2);
		t2_Node2->Connect(t2_Node3);
		t2_Node3->Connect(t2_Node4);
		t2_Node4->Connect(t2_Node2);
		t2_Node2->Disconnect(t2_Node3);

		TestEqual(TEXT("Correct Count Of Graphs"), t2_GraphsContainer->GetGraphs()->Num(), 1);
	}

	//Test for Bonds in Nodes
	{
		UGraphsContainer* t3_GraphsContainer = NewObject<UGraphsContainer>();

		UGraph* t3_Graph = UGraph::Create(t3_GraphsContainer);

		UGraphNode* t3_Node1 = UGraphNode::Create(t3_Graph);
		UGraphNode* t3_Node2 = UGraphNode::Create(t3_Graph);
		UGraphNode* t3_Node3 = UGraphNode::Create(t3_Graph);
		UGraphNode* t3_Node4 = UGraphNode::Create(t3_Graph);

		t3_Node1->Connect(t3_Node2);
		t3_Node2->Connect(t3_Node3);
		t3_Node3->Connect(t3_Node4);
		t3_Node4->Connect(t3_Node1);
		t3_Node4->Connect(t3_Node2);
		t3_Node3->Connect(t3_Node1);
		t3_Node1->Disconnect(t3_Node4);
		t3_Node2->Disconnect(t3_Node3);

		TestEqual(TEXT("Correct Count Of Bonds in Node1"), t3_Node1->GetBonds()->Num(), 2);
		TestEqual(TEXT("Correct Count Of Bonds in Node2"), t3_Node2->GetBonds()->Num(), 2);
		TestEqual(TEXT("Correct Count Of Bonds in Node3"), t3_Node3->GetBonds()->Num(), 2);
		TestEqual(TEXT("Correct Count Of Bonds in Node4"), t3_Node4->GetBonds()->Num(), 2);
	}

	//Test for Count of Nodes in Graph
	{
		UGraphsContainer* t4_GraphsContainer = NewObject<UGraphsContainer>();

		UGraph* t4_Graph = UGraph::Create(t4_GraphsContainer);

		UGraphNode* t4_Node1 = UGraphNode::Create(t4_Graph);
		UGraphNode* t4_Node2 = UGraphNode::Create(t4_Graph);
		UGraphNode* t4_Node3 = UGraphNode::Create(t4_Graph);
		UGraphNode* t4_Node4 = UGraphNode::Create(t4_Graph);

		TestEqual(TEXT("Correct Count Of Nodes in Graph"), t4_Graph->GetNodesPtr()->Num(), 4);
	}

	//Test for subtract and connect Graphs in one
	{
		UGraphsContainer* t5_GraphsContainer = NewObject<UGraphsContainer>();

		UGraph* t5_Graph = UGraph::Create(t5_GraphsContainer);

		UGraphNode* t5_Node1 = UGraphNode::Create(t5_Graph);
		UGraphNode* t5_Node2 = UGraphNode::Create(t5_Graph);
		UGraphNode* t5_Node3 = UGraphNode::Create(t5_Graph);
		UGraphNode* t5_Node4 = UGraphNode::Create(t5_Graph);

		t5_Node1->Connect(t5_Node2);
		t5_Node2->Connect(t5_Node3);
		t5_Node3->Connect(t5_Node4);
		t5_Node2->Disconnect(t5_Node3);
		t5_Node2->Connect(t5_Node3);

		TestEqual(TEXT("Correct Count Of Nodes in Graph"), t5_Graph->GetNodesPtr()->Num(), 4);
	}

	//Test for correct connect two Graphs
	{
		UGraphsContainer* t6_GraphsContainer = NewObject<UGraphsContainer>();

		UGraph* t6_Graph1 = UGraph::Create(t6_GraphsContainer);
		UGraph* t6_Graph2 = UGraph::Create(t6_GraphsContainer);

		UGraphNode* t6_Node1 = UGraphNode::Create(t6_Graph1);
		UGraphNode* t6_Node2 = UGraphNode::Create(t6_Graph1);
		UGraphNode* t6_Node3 = UGraphNode::Create(t6_Graph1);
		UGraphNode* t6_Node4 = UGraphNode::Create(t6_Graph2);
		UGraphNode* t6_Node5 = UGraphNode::Create(t6_Graph2);
		UGraphNode* t6_Node6 = UGraphNode::Create(t6_Graph2);
		UGraphNode* t6_Node7 = UGraphNode::Create(t6_Graph2);

		t6_Node1->Connect(t6_Node2);
		t6_Node2->Connect(t6_Node3);
		t6_Node4->Connect(t6_Node5);
		t6_Node5->Connect(t6_Node6);
		t6_Node6->Connect(t6_Node7);
		t6_Node3->Connect(t6_Node4);

		TestEqual(TEXT("Correct Count Of Nodes in Graph1"), t6_Graph1->GetNodesPtr()->Num(), 0);
		TestEqual(TEXT("Correct Count Of Nodes in Graph2"), t6_Graph2->GetNodesPtr()->Num(), 7);
	}

	return true;
}

#endif //WITH_DEV_AUTOMATION_TESTS