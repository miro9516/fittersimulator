#include "CoreTypes.h"
#include "Containers/UnrealString.h"
#include "MyObject.h"
#include "Misc/AutomationTest.h"

#if WITH_DEV_AUTOMATION_TESTS

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FMyObjectTest, "TestGroup.MyObjectTest", EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)

bool FMyObjectTest::RunTest(const FString& Parameters)
{
	{
		UMyObject* TestMyObject;
		TestMyObject = NewObject<UMyObject>();

		TestEqual(TEXT("CorrectSum"), TestMyObject->SomeSum(5, 6), 11);
	}

	return true;
}

#endif //WITH_DEV_AUTOMATION_TESTS