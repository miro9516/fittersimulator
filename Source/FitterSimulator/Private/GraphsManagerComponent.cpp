// Fill out your copyright notice in the Description page of Project Settings.


#include "GraphsManagerComponent.h"
#include "GraphsContainer.h"
#include "NodesMatcher.h"

// Sets default values for this component's properties
UGraphsManagerComponent::UGraphsManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	GraphsContainer = NewObject<UGraphsContainer>();
	NodesMatcher = NewObject<UNodesMatcher>();
}

// Called when the game starts
void UGraphsManagerComponent::BeginPlay()
{
	Super::BeginPlay();

}

UGraphsContainer* UGraphsManagerComponent::GetGraphsContainer()
{
	return GraphsContainer;
}

UNodesMatcher* UGraphsManagerComponent::GetNodesMatcher()
{
	return NodesMatcher;
}

