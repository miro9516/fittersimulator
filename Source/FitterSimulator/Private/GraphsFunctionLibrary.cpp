// Fill out your copyright notice in the Description page of Project Settings.


#include "GraphsFunctionLibrary.h"
#include "Graph.h"
#include "GraphNode.h"

UGraph* UGraphsFunctionLibrary::CreateGraph(UGraphsContainer* GraphContainer)
{
	return UGraph::Create(GraphContainer);
}

UGraphNode* UGraphsFunctionLibrary::CreateNode(UGraph* ParentGraph, UObject* Data)
{
	return UGraphNode::Create(ParentGraph, Data);
}
