// Fill out your copyright notice in the Description page of Project Settings.


#include "Graph.h"
#include "GraphsContainer.h"
#include "GraphNode.h"

UGraph* UGraph::Create(UGraphsContainer* NewGraphsContainer, TSet<UGraphNode*>* NodesToAdd)
{
	UGraph* Graph = NewObject<UGraph>();
	Graph->Init(NewGraphsContainer, NodesToAdd);
	return Graph;
}

UGraph* UGraph::Create(UGraphsContainer* NewGraphsContainer)
{
	TSet<UGraphNode*> NodesToAdd;
	return UGraph::Create(NewGraphsContainer, &NodesToAdd);
}

void UGraph::Init(UGraphsContainer* NewGraphsContainer, TSet<UGraphNode*>* NodesToAdd)
{
	this->GraphsContainer = NewGraphsContainer;

	Nodes.Append(*NodesToAdd);

	GraphsContainer->AddGraph(this);
}

void UGraph::AddNode(UGraphNode* Node)
{
	Nodes.Add(Node);
}

//TODO: Maybe Remove this Function
TSet<UGraphNode*>* UGraph::GetNodesPtr()
{
	return &Nodes;
}

TSet<UGraphNode*> UGraph::GetNodes()
{
	return Nodes;
}

UGraphsContainer* UGraph::GetGraphsContainer()
{
	return GraphsContainer;
}

void UGraph::NormalizeGraph()
{
	TSet<UGraph*> ExtractedGraphs;
	if (!NormalizeGraph(&Nodes, &ExtractedGraphs))
	{
		for (UGraph* Graph : ExtractedGraphs)
		{
			GraphsContainer->AddGraph(Graph);
			for (UGraphNode* Node : *(Graph->GetNodesPtr()))
			{
				Nodes.Remove(Node);
			}
		}
	}
}

bool UGraph::NormalizeGraph(TSet<UGraphNode*>* GraphsNodes, TSet<UGraph*>* ExtractedGraphs)
{
	TSet<UGraphNode*> MarkedNodes;
	for (auto Elem : *GraphsNodes)
	{
		UGraphNode* first = Elem;
		MarkedNodes.Add(first);

		PutBonds(first, &MarkedNodes);

		if (GraphsNodes->Num() == MarkedNodes.Num())
		{
			return true;
		}
		else
		{
			TSet<UGraphNode*> SubstractedNodes = Substract(GraphsNodes, &MarkedNodes);
			auto NewExtractedGraph = UGraph::Create(GraphsContainer, &SubstractedNodes);
			ExtractedGraphs->Add(NewExtractedGraph);
			for (auto Node : *NewExtractedGraph->GetNodesPtr())
			{
				Node->SetParentGraph(NewExtractedGraph);
			}
			return false;
		}
	}
	return true;
}

TSet<UGraphNode*> UGraph::Substract(TSet<UGraphNode*>* a, TSet<UGraphNode*>* b)
{
	TSet<UGraphNode*> ResultNodes;
	for (UGraphNode* Node : *a)
	{
		if (!(b->Contains(Node)))
		{
			ResultNodes.Add(Node);
		}
	}
	return ResultNodes;
}

void UGraph::PutBonds(UGraphNode* Node, TSet<UGraphNode*>* MarkedNodes)
{
	TSet<UGraphNode*>* Bonds = Node->GetBonds();
	for (UGraphNode* Bond : *Bonds)
	{
		if (!MarkedNodes->Contains(Bond))
		{
			MarkedNodes->Add(Bond);
			PutBonds(Bond, MarkedNodes);
		}
	}
}
