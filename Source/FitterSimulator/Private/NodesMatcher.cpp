// Fill out your copyright notice in the Description page of Project Settings.


#include "NodesMatcher.h"

TMap<UObject*, UGraphNode*> UNodesMatcher::GetObjectNodeMap()
{
	return ObjectNodeMap;
}

void UNodesMatcher::AddObjectNode(UObject* Object, UGraphNode* Node)
{
	ObjectNodeMap.Add(Object, Node);
}
