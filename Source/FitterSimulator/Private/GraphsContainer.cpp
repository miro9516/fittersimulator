// Fill out your copyright notice in the Description page of Project Settings.


#include "GraphsContainer.h"

void UGraphsContainer::AddGraph(UGraph* Graph)
{
	Graphs.Add(Graph);
}

TSet<UGraph*>* UGraphsContainer::GetGraphs()
{
	return &Graphs;
}

