// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GraphNode.generated.h"

class UGraph;

/**
 * 
 */
UCLASS(BlueprintType)
class FITTERSIMULATOR_API UGraphNode : public UObject
{
	GENERATED_BODY()
	
private:

	UPROPERTY()
	TSet<UGraphNode*> Bonds;

	UPROPERTY()
	UGraph* ParentGraph = nullptr;

	UPROPERTY()
	UObject* Data = nullptr;

	void Init(UGraph* NewParentGraph, UObject* NewData);

	void TransferNodesToGraph(UGraph* FromGraph, UGraph* ToGraph);

	void SetParentGraph(UGraph* NewParentGraph);

public:

	static UGraphNode* Create(UGraph* NewParentGraph);

	static UGraphNode* Create(UGraph* NewParentGraph, UObject* NewData);

	UFUNCTION(BlueprintCallable, Category = "Nodes")
	void Connect(UGraphNode* Node);

	UFUNCTION(BlueprintCallable, Category = "Nodes")
	void Disconnect(UGraphNode* Node);

	UFUNCTION(BlueprintPure, Category = "Nodes")
	UGraph* GetParentGraph();

	TSet<UGraphNode*>* GetBonds();

	UFUNCTION(BlueprintPure, Category = "Nodes")
	UObject* GetData();

	friend class UGraph;

};


