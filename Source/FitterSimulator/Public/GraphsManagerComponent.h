// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GraphsManagerComponent.generated.h"

class UGraphsContainer;
class UNodesMatcher;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FITTERSIMULATOR_API UGraphsManagerComponent : public UActorComponent
{
	GENERATED_BODY()

private:

	UPROPERTY()
	UGraphsContainer* GraphsContainer = nullptr;

	UPROPERTY()
	UNodesMatcher* NodesMatcher = nullptr;

public:	
	// Sets default values for this component's properties
	UGraphsManagerComponent();

	// Return Graphs Container
	UFUNCTION(BlueprintPure, Category = "GraphsManager")
	UGraphsContainer* GetGraphsContainer();

	// Return Nodes Matcher
	UFUNCTION(BlueprintPure, Category = "GraphsManager")
	UNodesMatcher* GetNodesMatcher();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
		
};
