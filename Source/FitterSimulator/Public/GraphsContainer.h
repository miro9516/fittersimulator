// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GraphsContainer.generated.h"

class UGraph;

UCLASS()
class FITTERSIMULATOR_API UGraphsContainer : public UObject
{
	GENERATED_BODY()
	
private:

	UPROPERTY()
	TSet<UGraph*> Graphs;

public:

	void AddGraph(UGraph* Graph);

	TSet<UGraph*>* GetGraphs();

};
