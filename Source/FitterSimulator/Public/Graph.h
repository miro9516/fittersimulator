// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Graph.generated.h"

class UGraphsContainer;
class UGraphNode;

/**
 * 
 */
UCLASS()
class FITTERSIMULATOR_API UGraph : public UObject
{
	GENERATED_BODY()
	
private:

	UGraphsContainer* GraphsContainer = nullptr;

	UPROPERTY()
	TSet<UGraphNode*> Nodes;

	void Init(UGraphsContainer* NewGraphsContainer, TSet<UGraphNode*>* NodesToAdd);

	bool NormalizeGraph(TSet<UGraphNode*>* GraphsNodes, TSet<UGraph*>* ExtractedGraphs);

	TSet<UGraphNode*> Substract(TSet<UGraphNode*>* a, TSet<UGraphNode*>* b);

	void PutBonds(UGraphNode* Node, TSet<UGraphNode*>* MarkedNodes);

public:

	static UGraph* Create(UGraphsContainer* NewGraphsContainer, TSet<UGraphNode*>* NodesToAdd);

	static UGraph* Create(UGraphsContainer* NewGraphsContainer);

	void AddNode(UGraphNode* Node);

	//TODO: Maybe Remove this function
	TSet<UGraphNode*>* GetNodesPtr();

	UFUNCTION(BlueprintPure, Category = "Graphs")
	TSet<UGraphNode*> GetNodes();

	void NormalizeGraph();

	UGraphsContainer* GetGraphsContainer();

};
