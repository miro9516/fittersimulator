// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GraphsFunctionLibrary.generated.h"

class UGraph;
class UGraphNode;
class UGraphsContainer;

/**
 * 
 */
UCLASS()
class FITTERSIMULATOR_API UGraphsFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, Category = "Graphs")
	static UGraph* CreateGraph(UGraphsContainer* GraphContainer);

	UFUNCTION(BlueprintCallable, Category = "Graphs")
	static UGraphNode* CreateNode(UGraph* ParentGraph, UObject* Data);
	
};
