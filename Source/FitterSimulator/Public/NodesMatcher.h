// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "NodesMatcher.generated.h"

class UGraphNode;

// Class for Matching GameObjects with Nodes in Graph Structure
UCLASS()
class FITTERSIMULATOR_API UNodesMatcher : public UObject
{
	GENERATED_BODY()

private:

	TMap<UObject*, UGraphNode*> ObjectNodeMap;

public:

	UFUNCTION(BlueprintPure, Category = "NodesMatching")
	TMap<UObject*, UGraphNode*> GetObjectNodeMap();

	UFUNCTION(BlueprintCallable, Category = "NodesMatching")
	void AddObjectNode(UObject* Object, UGraphNode* Node);
	
};
